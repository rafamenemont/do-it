import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
 *{
     margin: 0;
     padding: 0;
     box-sizing: border-box;
     outline:0;
 };

 :root{
    --white: #F5F5F5;
    --vanilla:  #F1EDE0;
    --black: #0C0D0D;
    --orange: #C85311;
    --gray: #666368;
    --red: #c53030;
 };

 body{
    background-color: var(--vanilla);
    color: var(---black);
 };

 button, input, body {
    font-family: 'PT Serif', serif;
    font-size: 1rem;
 };

 h1, h2, h3, h4, h5, h6 {
     font-family: 'Robot Mono', monospace;
     font-weight: 700;
 };
 
 button {
     cursor: pointer;
 };

 a {
     text-decoration: none;
 }

`;
