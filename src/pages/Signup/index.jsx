import Button from "../../components/Button";
import { Background, Container, Content, AnimationContainer } from "./styles";
import { Link, Redirect, useHistory } from "react-router-dom";
import Input from "../../components/input";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from '../../services/api' 
import { toast } from "react-toastify";

const Signup = ({ authenticated }) => {

  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório!"),
    email: yup.string().email("Email inválido").required("Campo obrigatório!"),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .required("Campo obrigatório!"),
    passwordConfirm: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas diferentes")
      .required("Campo obrigatório!"),
  });

  const history = useHistory()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });



  const onSubmitFunction = ({ name, email, password }) => {
    const user = { name, email, password};
    api.post("/user/register", user)
    .then((_) => {
        toast.dark('Sucesso ao criar a conta')
        return history.push('/login')
    })
    .catch((err) => toast.error("Erro ao criar a conta, tente novamente mais tarde"));
  };

  if (authenticated){
    return <Redirect to="/dashboard" />
  };

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunction)}>
            <h1>Cadastro</h1>
            <Input
              register={register}
              name="name"
              label="Nome"
              placeholder="Seu nome"
              error={errors.name?.message}
            ></Input>
            <Input
              register={register}
              name="email"
              label="Email"
              placeholder="Seu melhor email"
              error={errors.email?.message}

            ></Input>
            <Input
              register={register}
              name="password"
              type="password"
              label="Senha"
              placeholder="Uma senha bem segura"
              error={errors.password?.message}
              ></Input>
            <Input
              register={register}
              name="passwordConfirm"
              type="password"
              label="Confirmação da senha"
              placeholder="Confirmação da senha"
              error={errors.passwordConfirm?.message}
              ></Input>

            <Button type="submit">Enviar</Button>
            <p>
              Já tem uma conta? Faça seu <Link to="/login">login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
};

export default Signup;
