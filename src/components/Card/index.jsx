import React from 'react'
import Button from '../Button'
import { Container } from './styles'

function Card({title, date, onClick}) {
    return (
        <Container >
            <span>{title}</span>
            <hr/>
            <time>{date}</time>
            <Button onClick={onClick}>Concluir</Button>
        </Container>
    )
}

export default Card;
